$(function () {

    $('.birthday').mask('99/99/9999');
    $('.salary').mask("#.##0,00", {reverse: true});

    $('body').on('click', '.add_employee', function () {

        var t = $(this);
        var employees_box = t.parent().parent().parent().clone();
        var key = employees_box.data('key');
        key++;
        
        employees_box.find('.birthday').mask('99/99/9999');
        employees_box.find('.salary').mask("#.##0,00", {reverse: true});
        employees_box.find('input').val(null);
        employees_box.find('.rem_employee').removeClass('d-none');

        $('.employees_row').append(employees_box);

    });

    $('body').on('click', '.rem_employee', function () {
        
        var t = $(this);
        t.parent().parent().parent().remove();
        
    });

});